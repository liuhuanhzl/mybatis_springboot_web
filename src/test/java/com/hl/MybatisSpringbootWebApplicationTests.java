package com.hl;

import com.hl.entity.User;
import com.hl.mapper.UserMapper;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
class MybatisSpringbootWebApplicationTests {
	
	@Autowired
	private UserMapper userMapper;
	
	@Test
	void contextLoads() {
	}
	
	@Test
	public void testSelect(){
		//通过id查询
		User user = userMapper.selectById(1);
		System.out.println(user);

//		//查询全部对象 方法 findAll
//		List<User> all = userMapper.findAll();
//		for (User user : all) {
//			System.out.println(user);
//		}
//
//		//添加对象
//		User user1 = new User(8L, "唐僧", 25, "888888@126.com");
//		userMapper.save(user1);
//
//		//通过id查询
//		User user2 = userMapper.findById(2L);
//		System.out.println(user2);
//
//		//修改对象
//		User user = userMapper.findById(2L);
//		user.setName("lisa");
//		user.setAge(44);
//		user.setEmail("9999@qq.com");
//		userMapper.update(user);
//		System.out.println(userMapper.findById(2L));
//
//		//通过id删除
//		userMapper.deleteById(8L);
		
	}
	
}
