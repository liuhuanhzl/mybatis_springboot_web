package com.hl.requestBody;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RequestUser {
    @NotBlank(message = "姓名不能为空")
    private String name;
    private int age;
}
