package com.hl.response;

import lombok.Data;

import java.util.List;

@Data
public class Response<T> {
    
    private int code;

    private String msg;
    
    private T data;
    
    private List<T> list;
    
    public Response(int code){
        this.code = code;
        if (code == 200) {
            this.msg = "success";
        }
    }
    
    public Response(int code, String msg){
        this.code = code;
        this.msg = msg;
    }
    
}