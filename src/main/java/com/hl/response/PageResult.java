package com.hl.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class PageResult<T> {
    private int pageSize;
    private int totalCount;
    private int totalPage;
    private int currentPage;
    private List<T> resultList = new ArrayList();

    public PageResult(int currentPage, int pageSize) {
        if (currentPage <= 0) {
            this.currentPage = 1;
        } else {
            this.currentPage = currentPage;
        }
        if (pageSize < 0) {
            this.pageSize = 0;
        } else if (pageSize > 1000) {
            this.pageSize = 1000;
        } else {
            this.pageSize = pageSize;
        }
    }

    public int getTotalPage() {
        if (this.pageSize == 0) {
            return 0;
        } else {
            if (this.totalCount % this.pageSize == 0) {
                this.totalPage = this.totalCount / this.pageSize;
            } else {
                this.totalPage = this.totalCount / this.pageSize + 1;
            }

            return this.totalPage;
        }
    }

    public int getCurrentPage() {
        if (this.currentPage <= 0) {
            return 1;
        }
        return this.currentPage;
    }
}
