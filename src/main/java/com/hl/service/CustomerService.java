package com.hl.service;

import com.hl.entity.Customer;
import com.hl.response.Response;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface CustomerService {
    
    Response importCustomers(MultipartFile file) throws IOException;
    
    void addListCustomer(List<Customer> list);
    
    Customer queryByMemberId(String memberId);
    
    int countBigCustomer();
    
    int deleteAllBigCustomer();
    
}
