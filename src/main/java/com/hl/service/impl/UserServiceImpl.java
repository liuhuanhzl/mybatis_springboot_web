package com.hl.service.impl;

import com.hl.entity.User;
import com.hl.mapper.UserMapper;
import com.hl.requestBody.RequestUser;
import com.hl.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    
    @Autowired
    public UserMapper userMapper;
    
    @Override
    public User selectById(int id) {
        return userMapper.selectById(id);
    }
    
    @Override
    public List<User> getUserInfos(String name, int age) {
        return userMapper.getUserInfos(name, age);
    }
    
    @Override
    public List<User> getUserInfosExam(RequestUser requestUser) {
        return userMapper.getUserInfosExam(requestUser);
    }
    
    @Override
    public int saveUser(User user) {
        return userMapper.save(user);
    }
}
