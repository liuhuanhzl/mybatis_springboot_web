package com.hl.service;

import com.hl.entity.User;
import com.hl.requestBody.RequestUser;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface UserService {
    
    User selectById(int id);
    
    List<User> getUserInfos(String name, int age);
    
    List<User> getUserInfosExam(RequestUser requestUser);
    
    int saveUser(User user);
    
}
