package com.hl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@MapperScan("com.hl.mapper") //扫描的mapper
@SpringBootApplication
public class MybatisSpringbootWebApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(MybatisSpringbootWebApplication.class, args);
	}
	
}
