package com.hl.controller;

import com.hl.entity.Customer;
import com.hl.response.Response;
import com.hl.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/etf_customer")
@Slf4j
public class CustomerController {
    
    @Autowired
    private CustomerService customerService;
    
    @RequestMapping(value = "/get_user", method = RequestMethod.GET)
    public Response<Customer> getCustomer(@RequestParam String memberId) {
        Response<Customer> result = new Response(200);
        if (StringUtils.isBlank(memberId)) {
            return new Response<>(400, "memberId is null");
        }
        result.setData(customerService.queryByMemberId(memberId));
        return result;
    }
    
    @RequestMapping(value = "/import", method = RequestMethod.POST)
    public Response importCustomer(MultipartFile file) throws IOException {
        return customerService.importCustomers(file);
    }
    
}
