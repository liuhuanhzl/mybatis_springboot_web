package com.hl.entity;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class Customer {
    @ExcelIgnore
    private int id;
    
    @ExcelProperty("memberId")
    private String memberId;
}



/*
create table customer(
    id        int auto_increment primary key,
    member_id varchar(32) not null
);
 */