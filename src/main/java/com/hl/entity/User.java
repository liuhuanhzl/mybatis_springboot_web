package com.hl.entity;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class User {
    private Long id;
    private String name;
    private Integer age;
    private int gender;
    private String email;
    private BigDecimal total;
    private BigDecimal balance;
    private Date createTime;
    private Date updateTime;
    
    public String getGender() {
        if (gender == 0) return "男";
        return "女";
    }
}




/*
create table user
(
    id          bigint auto_increment primary key,
    name        varchar(30)     null,
    age         int             null,
    gender      int default 0,
    email       varchar(30)     null,
    total       decimal(40, 18) null,
    balance     decimal(40, 18) null,
    create_time timestamp       null,
    update_time timestamp       null
);
 */