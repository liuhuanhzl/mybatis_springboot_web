package com.hl.mapper;

import com.hl.entity.User;
import com.hl.requestBody.RequestUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {
    
    User selectById(int id);
    
    List<User> getUserInfos(@Param("name") String name,
                            @Param("age") int age);
    
    List<User> getUserInfosExam(@Param("body") RequestUser requestUser);
    
    int save(User user);
    
    int update(User user);
    
    int deleteById(Long id);
    
    List<User> findAll();
    
    User findById(Long id);
    
}
