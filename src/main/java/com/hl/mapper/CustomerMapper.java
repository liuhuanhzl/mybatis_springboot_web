package com.hl.mapper;

import com.hl.entity.Customer;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerMapper {
    
    int insertList(List<Customer> list);
    
    Customer queryByMemberId(String memberId);
    
    int countCustomer();
    
    int deleteAllCustomer();
}
